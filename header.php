<?php
 session_start();

   if(!isset($_SESSION['login']))
   {
   	   	$_SESSION['login'] = array();   	
   }

  require("include/config.php");
  require("include/db.php");
  require("include/functions.php");
  
  
  
 ?>

<!-- Start Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
					<div class="info">
                        <ul>
                            <li><a href="#"><i class="fa fa-clock-o"></i> Ponedeljak - Petak: 09:00 - 17:00 </a></li>
                            <li><a href="#"><i class="fa fa-home"></i> Vojvođanska 54, Sombor</a></li>
                            <li><a href="#"><i class="fa fa-phone"></i> 025/444 555</a></li>
                        </ul>
					</div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="share-icons">
						<ul>
                                <li><a target="_blank" href="https://twitter.com/?lang=sr"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="https://sr-rs.facebook.com/"><i class="fa fa-facebook"></i></a></li>                               
								<li><a target="_blank" href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>   
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Start Top Header Area -->
        
        <!-- Start Main Menu Area -->
        <div class="main-menu-area" data-spy="affix" data-offset-top="55">
            <div class="container"> 
                <div class="row"> 
                    <div class="col-md-3"> 
                        <div class="logo">
                            <a href="http://bojanmaricic.com"><img src="assets/img/logo.png" alt="Logo"></a>
                        </div>
                    </div>
                    
                    <div class="col-md-9"> 
                        <nav class="navbar navbar-default">
						
                            <!-- Mobile button -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar top-bar"></span>
                                    <span class="icon-bar middle-bar"></span>
                                    <span class="icon-bar bottom-bar"></span>
                                </button>
                            </div>

							<!-- Menu -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <div class="main-menu">
                                 <ul class="nav navbar-nav navbar-right">				
										<li><a href="http://bojanmaricic.com">Početna</a></li>
										<li><a href="/o-nama">O nama</a></li>										
										<li><a href="/usluge">Usluge</a></li>										
										<li><a href="/nas-tim">Naš tim</a></li> 
										<li><a href="/kontakt">Kontakt</a></li>
										<?php
											if(isset($_SESSION['id_user']))
											{
												$name = get_user_name($_SESSION['id_user']);
												
												echo "<li class=\"status-page\"><a href=\"zakazi-servis.php\" >Zakaži servis</a></li> ";
												echo "<li class=\"status-page\"><a href=\"status.php\" >Status</a></li> ";
												echo "<li class=\"login\"><a href=\"#\"><i class=\"fa fa-user\"></i> Korisnik: $name </a></li>";
												echo "<li class=\"login\" ><a href=\"logout.php\" ><i class=\"fa fa-sign-out\"></i> Odjavi se!</a></li> ";
											}
											else 
											{
												echo "<li class=\"login\"><a href=\"prijava.php\"><i class=\"fa fa-sign-in\"></i> Prijava/Registracija</a></li>";
											}
										?>							
									</ul>
                                </div>
                            </div>  
                        </nav>
                    </div>
                </div>
            </div>
        </div>
