<?php include('login.php'); ?>

<!DOCTYPE html>
<html lang="sh">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radnja | Prijava</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	
	<body>       
        <!-- Sign In Form Area -->
        <div class="content-block-area bg-gray">
            <div class="signup-form signin-form">	    
                <div class="logo_sign-in">
                    <a href="http://bojanmaricic.com">
                        <img src="assets/img/logo.png" alt="Logo">
                    </a>
                </div>
            
                <form action="prijava.php" method="post">
                    <h2>Prijavi se</h2>
                    
                        <div class="form-group-alert">
                        <?php if(isset($_GET['captchaPass'])){ ?>
                        <span>Uspešno ste registrovani, prijavite se na nalog</span>
                        <?php } ?>
                        </div>
                    
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="user" placeholder="Korisnicko ime" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="Lozinka" required="required">
                        </div>
                    </div>
                    <div class="form-group-alert">
					<span><?php include('errors.php'); ?><span>
					</div>
                    <div class="form-group">
                        <button type="submit" name="login" value="prijavi se" class="btn btn-primary btn-block btn-lg">Prijavi se</button>
                    </div>
                    <p class="text-center small">Nemate kreiran korisnički nalog?</p>
					<p class="text-center small"><a href="registracija.php"> Registruj se</a>.</p>
                </form>
                <p class="text-center">Vrati se na <a href="http://bojanmaricic.com">početnu</a> stranu.</p>
            </div>
        </div>
        <!-- End Sign In Form Area -->
        
        
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
		
	</body>

</html>