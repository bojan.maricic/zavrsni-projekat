<?php
session_start();

   require("include/config.php");
   include("include/db.php");


// initializing variables
$username = "";
$email    = "";
$firstname    = "";
$lastname    = "";
$errors = array();
 

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($connection, $_POST['username']);
  $email = mysqli_real_escape_string($connection, $_POST['email']);
  $firstname = mysqli_real_escape_string($connection, $_POST['firstname']);
  $lastname = mysqli_real_escape_string($connection, $_POST['lastname']);
  $password_1 = mysqli_real_escape_string($connection, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($connection, $_POST['password_2']);

 
  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username je obavezan"); }
  if (empty($email)) { array_push($errors, "Email je obavezan"); }
  if (empty($password_1)) { array_push($errors, "Password je obavezan"); }
  if ($password_1 != $password_2) {
	array_push($errors, "Unesene lozinke se ne slažu!");
  }
  // display valid captcha
  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $privatekey = "6Le-iWYUAAAAAP3T4r4DeJJczTiLgAReSkdb-a6N";

  $response = file_get_contents($url."?secret=".$privatekey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

  $data = json_decode($response);
  
  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($connection, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Ovo korisničko ime već postoji!");
    }

    if ($user['email'] === $email) {
      array_push($errors, "Ovaj email već postoji!");
    }
  }
  if(isset($data->success) AND $data->success==true AND count($errors)==0) { //checking if captcha is set and have no errors before
 
  // Finally, register user if there are no errors in the form
	$password = md5(SALT1."$password_1".SALT2);
	
  $query = "INSERT INTO users (username, email, password, firstname, lastname) 
  			    VALUES('$username', '$email', '$password', '$firstname', '$lastname' )";
  mysqli_query($connection,$query);

      $_SESSION['id_user'] = $user_id;
      $_SESSION['username'] = $username;
      header('location: prijava.php?captchaPass=true'); //redirect to prijava page
       
    }
  else {
  array_push($errors, "Captcha neuspešna, pokušajte ponovo!");
  }
}

?>