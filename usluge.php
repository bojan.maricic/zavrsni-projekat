<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica | Usluge</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	
	<body>
             
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
        
        <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>Usluge</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
        
        
        <!-- Start Services Price Area -->
		
		 <div class="content-block-area">
            <div class="container">
                <div class="row">

<?php

 $sql = "SELECT * FROM services WHERE 1";
 $result = mysqli_query($connection,$sql) or die(mysql_error());
   
  if (mysqli_num_rows($result)>0)
  {
   	   
     	while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
     	{
			
			echo "<div class=\"col-md-3 col-sm-6\">
                    <div class=\"pricingTable\">
						
						
			<div class=\"pricingTable-header\">	
					<span class=\"price-value\">$record[price]<span class=\"currency\">RSD</span></span>
                    <h3 class=\"title\">$record[service_type]</h3>			
				</div>
     	
     		<div class=\"pricing-content\">
                    <p>$record[service_description]</p>
				</div>
							
			                     
					</div>
                  </div>";								
     	}
  }
 ?>
              </div>                
            </div>
        </div>
		
		
		
      
        <!-- End Services-Price Area -->
        
        
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
        
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
		
	</body>

</html>