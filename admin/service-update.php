<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>Ažuriranje usluge</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="css/plugins/jquery-ui/jquery-ui.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">
	<!--font awesome-->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>

	<!-- jQuery UI -->
	<script src="js/plugins/jquery-ui/jquery-ui.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Theme scripts -->
	<script src="js/application.min.js"></script>

</head>

<body data-layout-topbar="fixed">
	
	
	<?php include ('header.html'); ?>
	
	<div class="container-fluid" id="content">


<?php include ('sidebar.html'); ?>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Usluge</h1>
					</div>
					<div class="pull-right">			
						<ul class="stats">
							<li class='lightred'>
								<i class="fa fa-calendar"></i>
								<div class="details">
									<span class="big">February 22, 2013</span>
									<span>Wednesday, 13:56</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box box-bordered box-color">
							<div class="box-title">
								<h3>
									<i class="fa fa-th-list"></i>
									Ažuriranje usluge
								</h3>
							</div>
							
							
<?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
        require('db_config.php');
        
        if(isset($_GET["service_id"]))
        $service_id = $_GET["service_id"];
    
        $sql = "SELECT services.service_id, services.service_type, services.service_description, services.price
        FROM services
        WHERE service_id = $service_id";
		
        $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
             
        if(mysqli_num_rows($result)>0)
        {
            while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
            {     
            $service_id = $row["service_id"];
            $service_type = $row["service_type"];
            $service_description = $row["service_description"];
            $price = $row["price"];
            }
           
            mysqli_free_result($result);
        }
        mysqli_close($connection);

?>								
							
							
							<div class="box-content nopadding">
								<form action="index.php?link=izmenjena-usluga" method="POST" class='form-horizontal form-bordered'>
									<div class="form-group">
										<label for="textfield" class="control-label col-sm-2">ID usluge</label>
										<div class="col-sm-6">
											<input type="text" name="service_id" class="form-control" value="<?php echo $service_id; ?>">
										</div>
									</div>
									<div class="form-group">
										<label for="textfield" class="control-label col-sm-2">Izmeni naziv usluge</label>
										<div class="col-sm-6">
											<input type="text" name="service_type" class="form-control" value="<?php echo $service_type; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni opis usluge</label>
										<div class="col-sm-6">
											<textarea type="text"  class="form-control" name="service_description" value=""><?php echo $service_description; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni cenu</label>
										<div class="col-sm-2">
											<input type="text" name="price" class="form-control" value="<?php echo $price; ?>">
										</div>
									</div>
									<div class="form-actions col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-primary">Unesi</button>
										<button type="button" class="btn" onClick="window.location.href='index.php?link=usluge';" >Otkaži</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		</div>

</body>

</html>

