<?php include ('sidebar.html'); ?>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Prijave</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="fa fa-table"></i>
									Podnešene prijave
								</h3>
							</div>
							<div class="box-content nopadding">							
	<?php
define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
require('db_config.php');

$sql = "SELECT problems.id_problems, problems.id_user, problems.plate_number, problems.select_services, problems.date, problems.time, problems.problem_description, problems.status
FROM problems";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{
echo "<table class=\"table table-hover table-nomargin table-bordered \">
	 <thead>
            <tr>
                <th>Id prijave</th>
				<th>Id korisnika</th>
                <th>Broj registracije</th>
                <th>id usluge</th>
                <th>Datum</th>
				<th>Vreme</th>
                <th>Opis problema</th>
                <th>Status vozila</th>
				<th>Opcije</th>
			</tr>
		</thead>	
			
			";


    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        
		$rowID = $row["id_problems"];
		echo "<tbody>";
        echo "<tr>";
        echo "<td>" . $row['id_problems'] . "</td>";  
		 echo "<td>" . $row['id_user'] . "</td>"; 
        echo "<td>" . $row['plate_number'] . "</td>";
        echo "<td>" . $row['select_services'] . "</td>";
		echo "<td>" . $row['date'] . "</td>";
        echo "<td>" . $row['time'] . "</td>";
		echo "<td>" . $row['problem_description'] . "</td>";
        echo "<td>" . $row['status'] . "</td>";
		echo "<td> <a href='problems-update.php?id_problems=$rowID' class=\"btn\" rel=\"tooltip\" title=\"Izmeni\"><i class=\"fa fa-edit\"></i></a>
		<a href='delete-problems.php?id_problems=$rowID' class=\"btn\" rel=\"tooltip\" title=\"Izbriši\" onclick=\"myFunction()\"><i class=\"fa fa-times\"></i></a></td>";
		
        echo "</tr>";
		echo "</tbody>";

    }
	echo "</table>";
    mysqli_free_result($result);
}

mysqli_close($connection);
?>																											
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			function myFunction() {
    			alert("Uspešno ste izbrisali prijavu!");
			}
		</script>