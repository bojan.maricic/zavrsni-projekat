<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>Ažuriranje prijave servisa</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="css/plugins/jquery-ui/jquery-ui.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">
	<!--font awesome-->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>

	<!-- jQuery UI -->
	<script src="js/plugins/jquery-ui/jquery-ui.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Theme scripts -->
	<script src="js/application.min.js"></script>
</head>

<body data-layout-topbar="fixed">
	
	
	<?php include ('header.html'); ?>
	
	<div class="container-fluid" id="content">


<?php include ('sidebar.html'); ?>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Prijava servisa</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box box-bordered box-color">
							<div class="box-title">
								<h3>
									<i class="fa fa-th-list"></i>
									Ažuriranje prijave servisa
								</h3>
							</div>
							
							
<?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
        require('db_config.php');
        
        if(isset($_GET["id_problems"]))
        $id_problems = $_GET["id_problems"];
    
$sql = "SELECT problems.id_problems, problems.plate_number, problems.select_services, problems.date, problems.time, problems.problem_description, problems.status
FROM problems
        WHERE id_problems = $id_problems";
		
        $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
             
        if(mysqli_num_rows($result)>0)
        {
            while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
            {     
            $id_problems = $row["id_problems"];
            $select_services = $row["select_services"];
			$date = $row["date"];
			$time = $row["time"];
            $status = $row["status"];
            }
           
            mysqli_free_result($result);
        }
        mysqli_close($connection);

?>								
							
							
							<div class="box-content nopadding">
								<form action="index.php?link=izmenjena-prijava-servisa" method="POST" class='form-horizontal form-bordered'>
									<div class="form-group">
										<label for="textfield" class="control-label col-sm-2">ID prijave servisa</label>
										<div class="col-sm-6">
											<input type="text" name="id_problems" class="form-control" value="<?php echo $id_problems; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni uslugu servisa</label>
										<div class="col-sm-6">
											<input type="number" class="form-control" name="select_services" value="<?php echo $select_services; ?>"></input>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni datum servisa</label>
										<div class="col-sm-2">
											<input type="date" name="date" class="form-control" value="<?php echo $date; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni vreme servisa</label>
										<div class="col-sm-2">
									<select name="time" class="form-control">
                                        <option value=""><?php echo $time; ?></option>
										<option value="09:00-10:00">09:00 - 10:00</option>
                                        <option value="10:00-11:00">10:00 - 11:00</option>
                                        <option value="11:00-12:00">11:00 - 12:00</option>
                                        <option value="12:00-13:00">12:00 - 13:00</option>
                                        <option value="13:00-14:00">13:00 - 14:00</option>
                                        <option value="14:00-15:00">14:00 - 15:00</option>
                                        <option value="15:00-16:00">15:00 - 16:00</option>
                                        <option value="16:00-17:00">16:00 - 17:00</option>                                   
                                    </select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Izmeni status vozila</label>
										<div class="col-sm-2">
											<input type="number" min="1" max="5" name="status" class="form-control" value="<?php echo $status; ?>">
										</div>
									</div>																	
									<div class="form-actions col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-primary">Unesi</button>
										<button type="button" class="btn" onClick="window.location.href='index.php?link=prijave';" >Otkaži</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		</div>

</body>

</html>

