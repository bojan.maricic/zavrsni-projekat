<?php include ('sidebar.html'); ?>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Usluge</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box box-bordered box-color">
							<div class="box-title">
								<h3>
									<i class="fa fa-th-list"></i>
									Unos nove usluge
								</h3>
							</div>
							<div class="box-content  nopadding">
								<form action="index.php?link=nova-unesena-usluga" method="POST" class='form-horizontal form-bordered' id="bb">
									<div class="form-group">
										<label for="textfield" class="control-label col-sm-2">Nova usluga</label>
										<div class="col-sm-6">
											<input type="text" name="service_type" class="form-control" data-rule-required="true">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Opis usluge</label>
										<div class="col-sm-6">
											<textarea type="text" name="service_description" class="form-control" data-rule-required="true"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Cena</label>
										<div class="col-sm-2">
											<input type="text" name="price" class="form-control" data-rule-required="true">
										</div>
									</div>
									

									<div class="form-actions col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-primary">Unesi</button>
										<button type="button" class="btn" onClick="window.location.href='index.php?link=usluge';" >Otkaži</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
