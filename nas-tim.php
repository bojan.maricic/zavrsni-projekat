<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica | Naš tim</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	<body>
        
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
       
        <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>Naš tim</h2>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
        
     
        
        <!-- Start Our team Area -->
        <div class="content-block-area">
            <div class="container">
               <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                       <div class="section-title text-center">
                           <h2><span>Naš iskusni</span> tim</h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p> Naš tim odlikuje dugogodišnje iskustvo u automehaničarskom poslu. Naš cilj je pružimo nakvalitetniju uslugu i učinimo da vaš automobil izgleda i funkcioniše perfektno, a vi budete zadovoljni našom uslugom.</p>
                       </div>
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-4 col-sm-6">
                        <div class="our-team">
                            <div class="our-team-image">
                                <img src="assets/img/mechanic1.jpg" alt="">  
                            </div>
                            <div class="team-info text-center">
                                <h3 class="name">Bojan Maričić</h3>
                                <span class="workplace">Automehaničar</span>
                            </div>
                        </div>
                    </div> 
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="our-team">
                            <div class="our-team-image">
                                <img src="assets/img/mechanic2.jpg" alt="">
                            </div>
                            <div class="team-info text-center">
                                 <h3 class="name">Miroslav Čojanović</h3>
                                <span class="workplace">Automehaničar</span>
                            </div>
                        </div>
                    </div> 

					<div class="col-md-4 col-sm-6">
                        <div class="our-team">
                            <div class="our-team-image">
                                <img src="assets/img/mechanic3.jpg" alt="">
                            </div>
                            <div class="team-info text-center">
                                 <h3 class="name">Petar Petrović</h3>
                                <span class="workplace">Automehaničar</span>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <!-- End Our team Area -->
       
        
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
		
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
		
	</body>

</html>