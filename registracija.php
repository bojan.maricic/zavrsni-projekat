<?php include('register.php') ?>
<!DOCTYPE html>
<html lang="sh">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radnja | Registracija</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
	
	<body>
        
        <!-- Sign Up Form Area -->
        <div class="content-block-area bg-gray">
            <div class="signup-form">
                <div class="logo_sign-in">
                    <a href="http://bojanmaricic.com">
                        <img src="assets/img/logo.png" alt="Logo">
                    </a>
                </div>
          
                <form  action="registracija.php" method="post">
				
                    <h2>Kreiraj korisnički nalog</h2>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="username" placeholder="korisničko ime" required oninvalid="this.setCustomValidity('Molimo vas, unesite korisničko ime')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" name="email" placeholder="email adresa" required oninvalid="this.setCustomValidity('Molimo vas, unesite email')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password_1" placeholder="lozinka" value="" required oninvalid="this.setCustomValidity('Molimo vas, unesite lozinku')" oninput="setCustomValidity('')">
                        </div>
                    </div> 
					
					<div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
							<i class="fa fa-lock"></i><i class="fa fa-check"></i></span>
                            <input type="password" class="form-control" name="password_2" placeholder="potvrdi lozinku" value="" required oninvalid="this.setCustomValidity('Molimo vas, potvrdite lozinku')" oninput="setCustomValidity('')">
                        </div>
                    </div> 
					<div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
                            <input type="text" class="form-control" name="firstname" placeholder="ime" value="" required oninvalid="this.setCustomValidity('Molimo vas, unesite vaše ime')" oninput="setCustomValidity('')">
                        </div>
                    </div> 
					
					<div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
                            <input type="text" class="form-control" name="lastname" placeholder="prezime" value="" required oninvalid="this.setCustomValidity('Molimo vas, unesite vaše prezime')" oninput="setCustomValidity('')">
                        </div>
                    </div> 							
                    <div class="form-group-captcha">                      
                        <div class="g-recaptcha" data-sitekey="6Le-iWYUAAAAAKpAFcqbNUEfpp8I9-DbX_-uh6hq" name="recaptcha"></div>
                    </div>		
					<div class="form-group-alert">
					<span><?php include('errors.php'); ?><span>
					</div> 	
                    <div class="form-group">
                        <button type="submit" name="reg_user" class="btn btn-primary btn-block btn-lg">Registruj se</button>
                    </div>
					
                    <p class="text-center small">Već imate korisnički nalog? </p>
					<p class="text-center small"><a href="prijava.php"> Prijavi se</a>.</p>
                </form>
                <p class="text-center">Vrati se na <a href="http://bojanmaricic.com">početnu</a> stranu.</p>
            </div>
        </div>
        <!-- End Sign Up Form Area -->
		
   
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
	</body>

</html>