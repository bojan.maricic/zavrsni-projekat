 <!-- Footer Area -->
        <footer class="site-footer">
            <!-- Footer Top Area -->
            <div class="footer-top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="footer-col">
                                <a href="index.html" class="footer-logo"><img src="assets/img/logo.png" alt="logo"></a>
                                <p>Pored ostalih kvalitetnih usluga za Vaš auto, polazimo od toga da je za ispravnost vozila najvažnije redovno odžavanje. Zato se naš centar bavi Auto mehanikom Vašeg auta. Uvereni smo da pored dugogodišnjeg iskustva, koje imamo, veliku važnost ima i ulaganje u tehničku opremljenost servisa.</p>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="footer-col footer-menu">
                                        <h3 class="footer-col-title">Navigacija</h3>
                                        <ul>
                                            <li><a href="http://bojanmaricic.com"><i class="fa fa-arrow-right"></i> Početna</a></li>
                                            <li><a href="/o-nama"><i class="fa fa-arrow-right"></i> O nama</a></li>
                                            <li><a href="/usluge"><i class="fa fa-arrow-right"></i> Usluge</a></li>
                                            <li><a href="/nas-tim"><i class="fa fa-arrow-right"></i> Naš tim</a></li>
                                            <li><a href="/kontakt"><i class="fa fa-arrow-right"></i> Kontakt</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="footer-col footer-menu">
                                        <h3 class="footer-col-title">Radno vreme</h3>
                                        <ul>
                                            <li> Ponedeljak: 09:00 - 17:00</li>
                                            <li> Utorak: 09:00 - 17:00</li>
                                            <li> Sreda: 09:00 - 17:00</li>
                                            <li> Četvrtak: 09:00 - 17:00</li>
                                            <li> Petak: 09:00 - 17:00</li>
                                            <li> Subota: neradan dan</li>
                                            <li> Nedelja: neradan dan</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="footer-col">
                                        <h3 class="footer-col-title">Lokacija</h3>                                    
                                        <div class="address-info">
											<span><i class="fa  fa-home"></i> Vojvođanska 54, Sombor</span><br>
                                            <span><i class="fa  fa-phone"></i> 025 555 444</span><br>
											<span><i class="fa  fa-mobile"></i> 063 11 55 55</span><br>
                                            <span><i class="fa  fa-envelope"></i> info@autoservis.com </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- End Footer Top -->

            <!-- Footer Bottom Area -->
            <div class="footer-copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-5">
                                &copy; Copyright <script type="text/javascript">
                                var d=new Date();
                                document.write(d.getFullYear());
                                </script> Sva prava zadržana.
                            </div>
                        <div class="col-md-6 col-sm-7 text-right">
                            Web izrada<a href="#"> Bojan</a> <span class="seprator">&amp;</span> <a href="#">Miroslav</a>
                        </div>
                    </div>
                </div>
            </div> <!-- End Footer Bottom Area -->
        </footer> <!-- End Footer Area -->