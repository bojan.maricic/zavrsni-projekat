<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica | O nama</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	
	<body>   
    
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
     
        <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>O nama</h2>                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
        
        <!-- Start Top Banner Area -->
        <div class="content-block-area bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="about-boxed-bg about-boxed-image"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="about-boxed">
                            <h2>Poverite nam <span>vaše vozilo!</span> Naša garancija je <span>naše iskustvo!!</span></h2>
                            <p class="subtitle">Preko 20 godina iskustva u ovoj branši garantuje Vam kvalitetnu uslugu!</p>
                            <p>Automehaničarska radionica poseduje servis opremljen savremenom opremom, stručnim i iskusnim osobljem koje će u što kraćem roku rešiti kvar na Vašem vozilu. Automehaničarska radionica postoji već dugi niz godina i ima iza sebe veliki broj odrađenih servisa i zadovoljnih klijenata. Zahvaljujući predanom radu, trudu i svakodnevim unapređenjem poslovanja Automehaničarske radionice napreduje iz dana u dan i stvara poseban odnos sa svojim klijentima, takav da što bolje razumemo Vaše potrebe i radimo na tome da Vama olakšamo ceo proces servisiranja. </p>
                            <div class="signature">
                                <img src="assets/img/signature.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Banner Area -->

        <!-- Start Our Parners Area -->
        <div class="content-block-area">
            <div class="container">
               <div class="row">
                   <div class="col-md-6 col-md-offset-3">
                       <div class="section-title text-center">
                           <h2><span>Naši</span> partneri</h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p>Mi smo ovlašćeni zastupnici i serviseri različitih proizvođača automobila za Republiku Srbiju. Ugrađujemo i prodajemo originalne auto delove za više tipova vozila.</p>

					  </div>
                   </div>
                </div>
                       
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/alfa.png" alt="Image">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/audi.png" alt="Image">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/bmw.png" alt="Image">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/mazda.png" alt="Image">
                                </div>
                            </div>	
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/mercedes.png" alt="Image">
                                </div>
                            </div>	
							<div class="col-md-2">
                                <div class="partners-logo">
                                    <img src="assets/img/toyota.png" alt="Image">
                                </div>
                            </div>
					    </div>
                    </div>
								
                </div>
            </div>
        </div>
        <!-- End Our Parners Area -->
        
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
        
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
		
	</body>

</html>