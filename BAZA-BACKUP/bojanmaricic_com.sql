-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars` (
  `car_id` int(6) NOT NULL AUTO_INCREMENT,
  `id_user` int(6) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `service_id` int(3) NOT NULL,
  `price` int(6) NOT NULL,
  `car_status` varchar(15) DEFAULT '1',
  PRIMARY KEY (`car_id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `problems`;
CREATE TABLE `problems` (
  `id_problems` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(5) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `select_services` varchar(20) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `problem_description` varchar(300) NOT NULL,
  `status` int(2) DEFAULT 1,
  PRIMARY KEY (`id_problems`),
  KEY `status` (`status`),
  CONSTRAINT `problems_ibfk_1` FOREIGN KEY (`status`) REFERENCES `status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `problems` (`id_problems`, `id_user`, `plate_number`, `select_services`, `date`, `time`, `problem_description`, `status`) VALUES
(6,	0,	'SO080OG',	'1',	'2018-07-23',	'11:00-12:00',	'problem sa kočnicama',	2),
(7,	0,	'SO033KK',	'1',	'2018-07-23',	'11:00-12:00',	'problem sa auspuhom',	1),
(11,	0,	'SO022AA',	'7',	'2018-07-26',	'13:00-14:00',	'Ne moÅ¾e da upali, treba reparirati elektroniku boÅ¡ pumpe.',	3),
(13,	0,	'SO001AA',	'7',	'2018-07-28',	'10:00-11:00',	'problem sa pumpom',	1),
(14,	0,	'NS000AA',	'3',	'2018-07-28',	'11:00-12:00',	'Popravka kvaÄila',	1),
(15,	0,	'SO055BB',	'8',	'2018-07-31',	'10:00-11:00',	'Poprvka amortizera',	1),
(16,	0,	'NS000AA',	'3',	'2018-07-28',	'11:00-12:00',	'Popravka kvaÄila',	1);

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `service_id` int(3) NOT NULL AUTO_INCREMENT,
  `service_type` varchar(20) NOT NULL,
  `service_description` text NOT NULL,
  `price` int(6) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `services` (`service_id`, `service_type`, `service_description`, `price`) VALUES
(1,	'Mali servis',	'Zamena motornog ulja, filtera vazduha i filtera klime',	4000),
(2,	'Veliki servis',	'Zamena zupčastog kaiša,rolera,španera i ostalih delova seta zupčenja.\r\nZamena vodene pumpe.',	15000),
(3,	'Popravka kvačila',	'Zamena seta kvačila, korpe, lamele, druk lagera. Zamena plivajućeg zamajca.',	50000),
(4,	'Zamena kočnica',	'Zamena kočnica, kočionog diska i kočione tečnosti. Testiranje ispravnosti kočnica.',	20000),
(5,	'Zamena seta guma',	'Zamena letnjih i zimskih guma. Skladištenje i čuvanje guma koji se ne koriste u sezoni.',	12000),
(6,	'Servis auspuha',	'Krpljenje auspuha i zadnjeg lonca, kvalitetna izrada, montaža i zamena izduvnih sistema na svim tipovima vozila.',	25000),
(7,	'Popravka boš pumpe',	'Reparacija boš pumpe i elektronike boš pumpe. Skidanje, rastavljanje, reparacija i ponovna montaža pumpe.',	45000),
(8,	'Popravka amortizera',	'Ugradnja novih, reparacija i punjenje vaših amortizera.',	25000),
(21,	'Pranje auta',	'Detaljno spoljašnje pranje i dubinsko pranje i čišćenje enterijera vašeg automobila.',	500),
(22,	'Popravka limarije',	'Popravka limarije i farbanje ',	10000);

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id_status` int(5) NOT NULL AUTO_INCREMENT,
  `car_status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `status` (`id_status`, `car_status`) VALUES
(1,	'primljeno'),
(2,	'u postupku'),
(3,	'problem rešen'),
(4,	'odnešen'),
(5,	'odbijeno');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id_user`, `username`, `password`, `email`, `firstname`, `lastname`) VALUES
(1,	'bojan',	'6a0762d51c3a98ac26fbe5a4a123106e',	'maricic.sombor@gmail.com',	'Bojan',	'Maricic'),
(2,	'olja',	'6a0762d51c3a98ac26fbe5a4a123106e',	'oljamaricic@gmail.com',	'Olja',	'Maricic'),
(33,	'nemanja',	'626894f82a324d31cd500d58ac087005',	'n@n.com',	'nemanja',	'b'),
(36,	'andrej',	'eaf4bd942049a9082225ef99c6c466ee',	'andrej@andrej.com',	'Andrej',	'Maricic'),
(43,	'petar',	'aa6d7f7c993b3a231753a6c1c4a81c49',	'petar@petar.com',	'petar',	'petrović'),
(48,	'dragan',	'712251f0459a8a27336683b2354fe54c',	'dragan@dragan.com',	'dragan',	'dragic'),
(49,	'jovan',	'eda66e0a81a8c0f592f4b00f4f02cf0f',	'jovan@jovan.com',	'jovan',	'jovanović'),
(50,	'marko',	'f4e21550982a9d988bc2fc6a37980716',	'marko@marko.com',	'marko',	'markovic'),
(51,	'stevan',	'125583dfbbbc90074fd2b2833bf5e3ae',	'stevan@stevan.com',	'stevan',	'stevan');

-- 2018-07-27 11:04:14
